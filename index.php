<?php
session_start();
unset($_SESSION);

$json_in = json_decode(file_get_contents('input2.json'), true);


function subTime($end, $start){
    $endT = strtotime($end);
    $startT = strtotime($start);

    if($start > $end){
        return false;
    }else{
        $dif = strftime("%H:%M", ($endT - $startT) );
        $split = explode(":", $dif);
        $h = $split[0];
        $m = $split[1];
        return $dif_min = ($h*60)+$m;
    }
}


function extraValidate($workload, $worked, $rest, $rested){
    $limit = $workload / 2;
    if($worked >= $limit){ // trabalhou mais que 50%
        if($rested < $rest){
            $hExtra = $rest - $rested;
        }else{
            $hExtra = 0;
        }
    }else{ 
            $hExtra = 0;
        }
    return $hExtra;
}



foreach($json_in['employees'] as $employee){ // Percorre todos os employees

    $pis = $employee['pis_number'];
    $_SESSION[$pis]['name']     = $employee['name'];
    $_SESSION[$pis]['pis']      = $employee['pis_number'];
    $_SESSION[$pis]['workload'] = $employee['workload']['workload_in_minutes'];
    $_SESSION[$pis]['workdays'] = count($employee['workload']['days']);
    $_SESSION[$pis]['rest']     = $employee['workload']['minimum_rest_interval_in_minutes'];
    $_SESSION[$pis]['c_day']    = 0;
    $_SESSION[$pis]['saldo']   = 0;

    foreach($employee['entries'] as $entry){ // Percorre todas as entries de employee
        $entry = strtotime(str_replace("T", " ", $entry)); // Troca o T por espaco e converte pra datetime
        $c_day = date('d', $entry); // Pega o dia da data atual do ponto
        $c_Month = date('m', $entry); // Pega o mes da data atual do ponto

        if($c_day == $_SESSION[$pis]['c_day']){ // Nao entra na 1a leitura, pois o dia nao eh igual a zero
            $r++;
            $_SESSION[$pis]['days'][$c_day][$r] = date("Y-m-d H:i:s", $entry);
        }else{ // Entra na primeira leitura
            $r = 1;
            $_SESSION[$pis]['c_day'] = $c_day;
            $_SESSION[$pis]['days'][$c_day]['entryMonth'] = $c_Month;
            $_SESSION[$pis]['days'][$c_day]['entryDay'] = $c_day;
            $_SESSION[$pis]['days'][$c_day][$r] = date("Y-m-d H:i:s", $entry);
        }
    }


    foreach($_SESSION as $timeCalc){
        $pis = $timeCalc['pis'];
        foreach($timeCalc['days'] as $counter){
            $cDay = $counter['entryDay'];
            $check = count($counter);
            switch ($check){
                case 6:
                    $period1 = subTime($counter[2], $counter[1]);
                    $period2 = subTime($counter[4], $counter[3]);
                    $interval = subTime($counter[3], $counter[2]);
                    if(!$period1 || !$period2){
                        $_SESSION[$pis]['days'][$cDay]['status'] = "Invalid entry";
                    }else{
                        $_SESSION[$pis]['days'][$cDay]['status'] = "OK";
                    }
                    $_SESSION[$pis]['days'][$cDay]['WORKED'] = $period1 + $period2;                    
                    $totalDay = $period1 + $period2 + extraValidate($_SESSION[$pis]['workload'], $_SESSION[$pis]['days'][$cDay]['WORKED'], $_SESSION[$pis]['rest'], $interval);
                    $_SESSION[$pis]['days'][$cDay]['totalDay'] = $totalDay;
                    $_SESSION[$pis]['days'][$cDay]['rested'] = $interval;
                    $_SESSION[$pis]['days'][$cDay]['balance'] = $totalDay - $_SESSION[$pis]['workload'];
                    $_SESSION[$pis]['saldo'] += $_SESSION[$pis]['days'][$cDay]['balance'];
                    unset($_SESSION[$pis]['days'][$cDay]['WORKED']);
                    unset($_SESSION[$pis]['days'][$cDay]['rested']);
                    break;
                case 5:
                    $period1 = subTime($counter[2], $counter[1]);
                    $period2 = $period1;
                    $interval = $_SESSION[$pis]['rest'];
                    $totalDay = ($period1 * 2) - $interval;
                    if(!$period1 || !$period2){
                        $_SESSION[$pis]['days'][$cDay]['status'] = "Invalid entry";
                    }else{
                        $_SESSION[$pis]['days'][$cDay]['status'] = "OK";
                    }
                    $_SESSION[$pis]['days'][$cDay]['WORKED'] = $totalDay;
                    $_SESSION[$pis]['days'][$cDay]['rested'] = $interval;
                    $_SESSION[$pis]['days'][$cDay]['balance'] = $totalDay - $_SESSION[$pis]['workload'];
                    $_SESSION[$pis]['saldo'] += $_SESSION[$pis]['days'][$cDay]['balance'];
                    unset($_SESSION[$pis]['days'][$cDay]['WORKED']);  
                    unset($_SESSION[$pis]['days'][$cDay]['rested']);                  
                    break;
                case 4:
                    $period1 = subTime($counter[2], $counter[1]);
                    $totalDay = $period1;
                    $interval = 0;
                    if(!$period1){
                        $_SESSION[$pis]['days'][$cDay]['status'] = "Invalid entry";
                    }else{
                        $_SESSION[$pis]['days'][$cDay]['status'] = "OK";
                    }
                    $_SESSION[$pis]['days'][$cDay]['period1'] = $period1;
                    $_SESSION[$pis]['days'][$cDay]['WORKED'] = $totalDay;
                    $_SESSION[$pis]['days'][$cDay]['rested'] = $interval;
                    $_SESSION[$pis]['days'][$cDay]['balance'] = $totalDay - $_SESSION[$pis]['workload'];
                    $_SESSION[$pis]['saldo'] += $_SESSION[$pis]['days'][$cDay]['balance'];
                    unset($_SESSION[$pis]['days'][$cDay]['WORKED']); 
                    unset($_SESSION[$pis]['days'][$cDay]['rested']);                   
                    break;  
                case 3:
                    $totalDay = $_SESSION[$pis]['workload'] / 2;
                    $_SESSION[$pis]['days'][$cDay]['WORKED'] = $totalDay;
                    $_SESSION[$pis]['days'][$cDay]['balance'] = $totalDay - $_SESSION[$pis]['workload'];
                    $_SESSION[$pis]['saldo'] += $_SESSION[$pis]['days'][$cDay]['balance'];
                    unset($_SESSION[$pis]['days'][$cDay]['WORKED']);     
                    unset($_SESSION[$pis]['days'][$cDay]['rested']);               
                    break;                    
                default:
                    if($check > 11){
                        $_SESSION[$pis]['days'][$cDay]['status'] = "Invalid entry";
                    }
                    break;
            }
        }
        unset($_SESSION[$pis]['c_day']);
    }

}


$file = fopen('saida.json', 'w');
fwrite($file, json_encode($_SESSION));
fclose($file);
