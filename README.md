O script analisa o JSON fornecido e faz as seguintes validações:

- Marcações de ponto que estejam em ordem cronológica incorreta ele marcará como "invalid entry"
- Para os dias que houver apenas 3 marcações (entrada, saída intervalo, entrada intervalo), ele atribui ao tempo do 2o período o mesmo tempo trabalhado no 1o período.
- Para os dias que houver apenas 2 marcações, será considerado apenas o tempo trabalhado no 1o período e não será considerado nenhum tempo de intervalo.
- Para os dias que houver apenas 1 marcação, será atribuido o valor de metade da carga diária de tempo.

